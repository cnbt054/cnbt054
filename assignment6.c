#include<stdio.h>
int main()
{
int fact=1,n,i;
printf("Enter the number\n");
scanf("%d",&n);
if(n==0)
{
fact=1;
}
else
{
for(i=1;i<=n;i++)
fact=fact*i;
}
printf("Factorial of number=%d\n",fact);
return 0;
}
Output
Enter the number                                                                                                                       
5                                                                                                                                      
Factorial of number=120

#include <stdio.h>

int main()
{
int temp,n;
printf("Enter number:\n");
scanf("%d",&n);
temp=n;
while(n!=0)
{
    temp=n%10;
    printf("%d\t",temp);
    n=n/10;
}
return 0;
}
Output
Enter number:                                                                                                                          
576                                                                                                                                    
6       7       5 


#include <stdio.h>

int main()
{
float i,n,sum=0.0;
printf("Enter number:\n");
scanf("%f",&n);
for(i=1;i<=n;i++)
{
    sum=sum+1/(i*i);
}
printf("Sum of series=%f\n",sum);
return 0;
}
Output
Enter number:                                                                                                                          
5                                                                                                                                      
Sum of series=1.463611 


#include <stdio.h>

int main()
{
int i,j;
for(i=1;i<=5;i++)
{
    printf("\n");
    for(j=1;j<=i;j++)
    printf("%d",i);
}
return 0;
}
Output
1                                                                                                                                      
22                                                                                                                                     
333                                                                                                                                    
4444                                                                                                                                   
55555


#include <stdio.h>

int main()
{
int i,j;
char ch;
for(i=65;i<=70;i++)
{
    printf("\n");
    for(j=65;j<=i;j++)
    printf("%c",j);
}
return 0;
}
Output
A
AB                                                                                                                                     
ABC                                                                                                                                    
ABCD                                                                                                                                   
ABCDE                                                                                                                                  
ABCDEF 


#include <stdio.h>
int main()
{
    int n1, n2, i, gcd;

    printf("Enter two integers: ");
    scanf("%d %d", &n1, &n2);

    for(i=1; i <= n1 && i <= n2; ++i)
    {
    
        if(n1%i==0 && n2%i==0)
            gcd = i;
    }

    printf("G.C.D of %d and %d is %d", n1, n2, gcd);

    return 0;
}
Output
Enter two integers: 12                                                                                                                 
8                                                                                                                                      
G.C.D of 12 and 8 is 4 


