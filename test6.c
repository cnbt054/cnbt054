#include<stdio.h>
int main()
{
    int m,n,i,j,matrix[10][10],transpose[10][10];
    printf("Enter number of rows and columns of a matrix\n");
    scanf("%d%d",&m,&n);
    printf("Enter elements of the matrix\n");
    for(i=0;i<m;i++)
    for(j=0;j<n;j++)
    scanf("%d",&matrix[i][j]);
    for(i=0;i<m;i++)
    for(j=0;j<n;j++)
    transpose[j][i]=matrix[i][j];
    printf("Transpose of matrix:\n");
    for(i=0;i<n;i++)
    {
        for(j=0;j<m;j++)
        printf("%d\t",transpose[i][j]);
        printf("\n");
    }
    return 0;
}
Output:
Enter number of rows and columns of a matrix                                                                                           
3 3                                                                                                                                    
Enter elements of the matrix                                                                                                           
1 2 3                                                                                                                                  
4 5 6                                                                                                                                  
7 8 9                                                                                                                                  
Transpose of matrix:
1       4       7                                                                                                                      
2       5       8                                                                                                                      
3       6       9 //write your code here