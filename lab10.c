#include<stdio.h>
#include<stdlib.h>
int main()
{
    FILE *f1;
    char str1[150];
    f1=fopen("INPUT","w");
    if (f1 == NULL)
    {
        printf("Error!");
        exit(1);
    }
    printf("Enter a sentence\n");
    gets(str1);
    fprintf(f1,"%s",str1);
    fclose(f1);
    f1=fopen("INPUT","r");
    fscanf(f1,"%[^\n]",str1);
    printf("Data from the file:\n");
    puts(str1);
    fclose(f1);
    return 0;
}

Output:
Enter a sentence                                                                                                                       
hello my name is amaan                                                                                                                 
Data from the file:                                                                                                                    
hello my name is amaan 