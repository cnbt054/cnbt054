#include <stdio.h>
#define SIZE 100

int main()

{

int arr[SIZE], i, n, ele, pos;

printf("Enter size of the array : ");

scanf("%d", &n);

printf("Enter array elements\n");

for(i=0; i<n; i++)

{

scanf("%d", &arr[i]);

}

printf("Enter element to be inserted : ");

scanf("%d", &ele);

printf("Enter position at which element to be inserted ");

scanf("%d", &pos);

for(i=n; i>=pos; i--)

{

arr[i] = arr[i-1];

}

arr[pos-1] = ele;

n=n+1;

printf("Array elements after insertion : ");

for(i=0; i<n+1; i++)

{

printf("%d\n", arr[i]);

}
return 0;
}

Enter size of the array : 5                                                                                                            
Enter array elements
1
2                                                                                                                                      
3
4
5
Enter element to be inserted : 10                                                                                                      
Enter position at which element to be inserted 3                                                                                      
Array elements after insertion : 1
2
10
3
4
5

                                                                                                                                                             //write your code here