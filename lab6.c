#include<stdio.h>

int main()

{ 
   int i,j,marks[5][3],max_marks;
   for(i=0;i<5;i++)
   {
   printf("Enter marks obtained by student %d",i);
   for(j=0;j<3;j++)
   {
       printf("\n marks[%d][%d]=",i,j);
       scanf("%d",&marks[i][j]);
   }
   }
   for(j=0;j<3;j++)
   {
       max_marks=marks[0][j];
       for(i=1;i<5;i++)
       {
           if(marks[i][j]>max_marks)
           max_marks=marks[i][j];
       }
       printf("\n The highest marks obtained in subject %d=%d",j,max_marks);
   }
   return 0;
}
Output:

Enter marks obtained by student 0                                                                                                      
 marks[0][0]=89                                                                                                                        
                                                                                                                                       
 marks[0][1]=76                                                                                                                        
                                                                                                                                       
 marks[0][2]=100                                                                                                                       
Enter marks obtained by student 1
marks[1][0]=99                                                                                                                        
                                                                                                                                       
 marks[1][1]=90                                                                                                                        
                                                                                                                                       
 marks[1][2]=89                                                                                                                        
Enter marks obtained by student 2                                                                                                      
 marks[2][0]=67               
 
 marks[2][1]=76                                                                                                                        
                                                                                                                                       
 marks[2][2]=56                                                                                                                        
Enter marks obtained by student 3                                                                                                      
 marks[3][0]=88                                                                                                                        
                                                                                                                                       
 marks[3][1]=77 
 
 marks[3][2]=66                                                                                                                        
Enter marks obtained by student 4                                                                                                      
 marks[4][0]=67                                                                                                                        
                                                                                                                                       
 marks[4][1]=78                                                                                                                        
                                                                                                                                       
 marks[4][2]=89
 
 The highest marks obtained in subject 0=99                                                                                            
 The highest marks obtained in subject 1=90                                                                                            
 The highest marks obtained in subject 2=100
//write your code here