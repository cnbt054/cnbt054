#include <stdio.h>
int main()
{
    char str[100];
    char *ptr;
    printf("Enter a character: ");
    scanf("%c",str);
    ptr=str;
    if(*ptr=='A' ||*ptr=='E' ||*ptr=='I' ||*ptr=='O' ||*ptr=='U' ||*ptr=='a' ||*ptr=='e' ||*ptr=='i' ||*ptr=='o' ||*ptr=='u')
    printf("Entered Character is a vowel");
    else
    printf("Entered Character is not a vowel");
    return 0;
}
Output:
Enter a character: E                                                                                                                   
Entered Character is a vowel  


#include<stdio.h>
void swap(int *a,int *b);
int main()
{

int num1,num2;

printf("Enter the First Number:");

scanf("%d",&num1);

printf("Enter the Second Number:");

scanf("%d",&num2);

printf("Before Swapping the two numbers are %dand%d\n",num1,num2);

swap(&num1,&num2);

printf("After Swapping the two numbers are %dand%d\n",num1,num2);

return 0;

} 

void swap( int *a, int *b)
{

int temp;

temp=*a;

*a=*b;

*b=temp;

}
Output:
Enter the First Number:10                                                                                                              
Enter the Second Number:20                                                                                                             
Before Swapping the two numbers are 10and20                                                                                            
After Swapping the two numbers are 20and10